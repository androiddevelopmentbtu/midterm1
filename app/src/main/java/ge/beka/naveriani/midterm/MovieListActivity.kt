package ge.beka.naveriani.midterm

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import ge.beka.naveriani.midterm.external.ServiceBuilder
import ge.beka.naveriani.midterm.external.TmdbEndpoints
import ge.beka.naveriani.midterm.models.ItemModel
import ge.beka.naveriani.midterm.models.response.PopularMovies
import kotlinx.android.synthetic.main.activity_movie_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieListActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    val items = ArrayList<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list)
        init()
    }

    private fun init(){
        movieView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items, this)
        movieView.adapter = adapter
        auth = FirebaseAuth.getInstance()

        signOutButton.setOnClickListener {
            signOut()
        }

        loadExamples()
    }

    private fun signOut(){
        auth.signOut()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }


    private fun addItem(itemData: ItemModel){
        items.add(itemData)
        adapter.notifyItemInserted(items.size - 1)
    }

    private fun loadExamples(){
        val tmdbApi = ServiceBuilder.buildService(TmdbEndpoints::class.java)
        val response = tmdbApi.getMovies("05b74ca66d31909362cea3fbadb406cf")

        response.enqueue(object : Callback<PopularMovies> {
            override fun onResponse(call: Call<PopularMovies>, response: Response<PopularMovies>) {
                val movies = response.body()!!.results
                for (movie in movies) {
                    addItem(ItemModel(movie.poster_path, movie.title, movie.overview, movie.release_date))
                }
            }
            override fun onFailure(call: Call<PopularMovies>, t: Throwable) {
                Log.d("tmdbApCall", "Error occurred while getting data from tmdbApi")
            }
        })

    }

}
