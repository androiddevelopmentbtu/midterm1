package ge.beka.naveriani.midterm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()

        signUpButtonFirebase.setOnClickListener {
           signUp()
        }
    }

    private fun signUp(){

        val email = emailInputFirebase.text.toString()
        val password = passwordInputFirebase.text.toString()

        if (email.isEmpty()){
            emailInputFirebase.error = "Please provide email"
            emailInputFirebase.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailInputFirebase.error = "Invalid email format"
            emailInputFirebase.requestFocus()
            return
        }

        if (password.isEmpty()){
            passwordInputFirebase.error = "Please provide passowrd"
            passwordInputFirebase.requestFocus()
            return
        }

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                } else {
                    Toast.makeText(baseContext, "Error occurred while signing up, please try again later",
                    Toast.LENGTH_SHORT).show()
                }
            }

    }

}
