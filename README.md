# Author: Beka Naveriani

# Moive List Midterm

[![N|Solid](https://cdn.gweb.ge/buffer/1001285/pictures/logo/022fe5525cb08c8efad4d879005b131f.png)]()

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Movie list app for btu midterm.

  - Sign in activity
  - Sign up activity
  - Movies list activity

# Features!

  - Google Firebase Authentication
  - https://www.themoviedb.org/ API data


You can:
  - Get all popular movie lise from API

### Tech

Used Technologies

* [Glind] - For render images from api
* [Retrofit] - For rest calls
* [TheMovieDB API] - To fetch movies data.
* [Kotlin] - To write app

Project is uploaded on [public repository](https://bitbucket.org/androiddevelopmentbtu/midterm1/src/master/) on BitBucket.